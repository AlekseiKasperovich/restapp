package com.example.rest.model;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class Report {

    private List<Task> tasks = new ArrayList<>();

    public Report() {
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    @Override
    public String toString() {
        return "Report{" + "tasks=" + tasks + '}';
    }

}
