package com.example.rest.repository;

import com.example.rest.model.Task;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface TaskRepository extends JpaRepository<Task, Integer> {

    List<Task> findTaskByNameContaining(String name);

    List<Task> findTaskByStatus(Integer status);

    @Query("SELECT t FROM Task t WHERE t.startDate >= :date1 AND t.endDate <= :date2")
    List<Task> findTaskByDate(@Param("date1") Date date1, @Param("date2") Date date2);
}
