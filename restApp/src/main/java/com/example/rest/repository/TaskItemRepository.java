package com.example.rest.repository;

import com.example.rest.model.TaskItem;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TaskItemRepository extends JpaRepository<TaskItem, Integer> {

    List<TaskItem> findTaskItemByValueContaining(String value);

    List<TaskItem> findTaskItemByTaskTaskId(Integer id);

}
