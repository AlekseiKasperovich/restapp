package com.example.rest.service;

import com.example.rest.model.TaskItem;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public interface TaskItemService {

    TaskItem saveTaskItem(TaskItem item);

    List<TaskItem> getTaskItems();

    TaskItem getTaskItemById(Integer id);

    List<TaskItem> getTaskItemsByValueContaining(String value);

    List<TaskItem> getTaskItemsByTaskId(Integer id);

    void deleteTaskItem(Integer id);

    TaskItem updateTaskItem(TaskItem item);
}
