package com.example.rest.service;

import com.example.rest.handler.FileHandler;
import com.example.rest.model.Report;
import com.example.rest.model.Task;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReportServiceImpl implements ReportService {

    @Autowired
    FileHandler fileHandler;

    @Override
    public String save(List<Task> tasks) {
        Report report = new Report();
        report.setTasks(tasks);
        return fileHandler.writeToXLSX(report);
    }
}
