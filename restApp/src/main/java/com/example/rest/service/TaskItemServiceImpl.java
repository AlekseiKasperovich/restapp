package com.example.rest.service;

import com.example.rest.model.Task;
import com.example.rest.model.TaskItem;
import com.example.rest.repository.TaskItemRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TaskItemServiceImpl implements TaskItemService {

    @Autowired
    private TaskItemRepository repository;

    @Autowired
    private TaskService taskService;

    @Transactional
    @Override
    public TaskItem saveTaskItem(TaskItem item) {
        return repository.save(item);
    }

    @Transactional(readOnly = true)
    @Override
    public List<TaskItem> getTaskItems() {
        return repository.findAll();
    }

    @Transactional(readOnly = true)
    @Override
    public TaskItem getTaskItemById(Integer id) {
        return repository.findById(id).orElse(null);
    }

    @Transactional(readOnly = true)
    @Override
    public List<TaskItem> getTaskItemsByValueContaining(String value) {
        return repository.findTaskItemByValueContaining(value);
    }

    @Transactional(readOnly = true)
    @Override
    public List<TaskItem> getTaskItemsByTaskId(Integer id) {
        return repository.findTaskItemByTaskTaskId(id);
    }

    @Transactional
    @Override
    public void deleteTaskItem(Integer id) {
        List<Task> tasks = taskService.getTasks();
        for (int i = 0; i < tasks.size(); i++) {
            Task task = tasks.get(i);
            for (int j = 0; j < task.getItems().size(); j++) {
                TaskItem item = task.getItems().get(j);
                if (item.getItemId().equals(id)) {
                    task.removeItem(item);
                }
            }
        }
        repository.deleteById(id);
    }

    @Transactional
    @Override
    public TaskItem updateTaskItem(TaskItem item) {
        TaskItem newItem = getTaskItemById(item.getItemId());
        if (newItem == null) {
            return saveTaskItem(item);
        }
        newItem.setName(item.getName());
        newItem.setDescription(item.getDescription());
        newItem.setValue(item.getValue());
        return saveTaskItem(newItem);
    }
}
