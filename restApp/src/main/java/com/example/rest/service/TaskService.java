package com.example.rest.service;

import com.example.rest.model.Task;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public interface TaskService {

    Task saveTask(Task task);

    List<Task> getTasks();

    Task getTaskById(Integer id);

    List<Task> getTasksByNameContaining(String name);

    List<Task> getTasksByStatus(Integer status);

    List<Task> getTasksByDate(String date1, String date2);

    void deleteTask(Integer id);

    Task updateTask(Task task);

    Task addItemToTask(Task task, Integer itemId);

    Task removeItemFromTask(Task task, Integer itemId);
}
