package com.example.rest.service;

import com.example.rest.model.Task;
import com.example.rest.model.TaskItem;
import com.example.rest.repository.TaskRepository;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TaskServiceImpl implements TaskService {

    private static final Logger LOGGER = LogManager.getLogger(TaskServiceImpl.class);

    @Autowired
    private TaskRepository repository;

    @Autowired
    private TaskItemService itemService;

    @Transactional
    @Override
    public Task saveTask(Task task) {
        return repository.save(task);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Task> getTasks() {
        return repository.findAll();
    }

    @Transactional(readOnly = true)
    @Override
    public Task getTaskById(Integer id) {
        return repository.findById(id).orElse(null);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Task> getTasksByNameContaining(String name) {
        return repository.findTaskByNameContaining(name);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Task> getTasksByStatus(Integer status) {
        return repository.findTaskByStatus(status);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Task> getTasksByDate(String date1, String date2) {
        Date newDate1 = null;
        Date newDate2 = null;
        try {
            newDate1 = new SimpleDateFormat("yyyy-MM-dd").parse(date1);
            newDate2 = new SimpleDateFormat("yyyy-MM-dd").parse(date1);
        } catch (ParseException ex) {
            LOGGER.error(ex);
        }
        return repository.findTaskByDate(newDate1, newDate2);
    }

    @Transactional
    @Override
    public void deleteTask(Integer id) {
        repository.deleteById(id);
    }

    @Transactional
    @Override
    public Task updateTask(Task task) {
        Task newTask = getTaskById(task.getTaskId());
        if (newTask == null) {
            return saveTask(task);
        }
        newTask.setName(task.getName());
        newTask.setDescription(task.getDescription());
        newTask.setStartDate(task.getStartDate());
        newTask.setEndDate(task.getEndDate());
        newTask.setStatus(task.getStatus());
        return saveTask(newTask);
    }

    @Transactional
    @Override
    public Task addItemToTask(Task task, Integer itemId) {
        TaskItem item = itemService.getTaskItemById(itemId);
        if (item == null) {
            return null;
        }
        for (int i = 0; i < task.getItems().size(); i++) {
            TaskItem addedItem = task.getItems().get(i);
            if (addedItem.getItemId().equals(itemId)) {
                return task;
            }
        }
        TaskItem newItem = new TaskItem();
        newItem.setName(item.getName());
        newItem.setDescription(item.getDescription());
        newItem.setValue(item.getValue());
        task.addItem(newItem);
        itemService.saveTaskItem(newItem);
        return task;
    }

    @Transactional
    @Override
    public Task removeItemFromTask(Task task, Integer itemId) {
        TaskItem item = itemService.getTaskItemById(itemId);
        if (item == null) {
            return null;
        }
        for (int i = 0; i < task.getItems().size(); i++) {
            item = task.getItems().get(i);
            if (item.getItemId().equals(itemId)) {
                task.removeItem(item);
                return task;
            }
        }
        return null;
    }
}
