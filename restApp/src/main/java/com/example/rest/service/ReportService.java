package com.example.rest.service;

import com.example.rest.model.Task;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public interface ReportService {

    String save(List<Task> tasks);
}
