package com.example.rest.controller;

import com.example.rest.model.TaskItem;
import com.example.rest.service.TaskItemService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/items")
@RestController
public class TaskItemRestController {

    @Autowired
    private TaskItemService service;

    @PostMapping
    public ResponseEntity<TaskItem> addTaskItem(@RequestBody TaskItem item) {
        return new ResponseEntity(service.saveTaskItem(item), HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<List<TaskItem>> findAllTaskItems() {
        return new ResponseEntity(service.getTaskItems(), HttpStatus.OK);
    }

    @GetMapping("value/{value}")
    public ResponseEntity<List<TaskItem>> findAllTaskItemsByValueContaining(@PathVariable String value) {
        return new ResponseEntity(service.getTaskItemsByValueContaining(value),
                HttpStatus.OK);
    }

    @GetMapping("task/{id}")
    public ResponseEntity<List<TaskItem>> findAllTaskItemsByTaskId(@PathVariable Integer id) {
        return new ResponseEntity(service.getTaskItemsByTaskId(id), HttpStatus.OK);
    }

    @GetMapping("{id}")
    public ResponseEntity<TaskItem> findTaskItemById(@PathVariable Integer id) {
        TaskItem item = service.getTaskItemById(id);
        if (item == null) {
            return new ResponseEntity("Task Item by ID " + id + " not found!",
                    HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(item, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<TaskItem> updateTaskItem(@RequestBody TaskItem item) {
        return new ResponseEntity(service.updateTaskItem(item), HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public ResponseEntity deleteTaskItem(@PathVariable Integer id) {
        TaskItem item = service.getTaskItemById(id);
        if (item == null) {
            return new ResponseEntity("Task Item by ID " + id + " not found!",
                    HttpStatus.NOT_FOUND);
        }
        service.deleteTaskItem(id);
        return new ResponseEntity("Task Item " + id + " removed!", HttpStatus.OK);
    }
}
