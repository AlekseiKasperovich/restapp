package com.example.rest.controller;

import com.example.rest.service.ReportService;
import com.example.rest.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/report")
@RestController
public class ReportRestController {

    @Autowired
    private ReportService reportService;

    @Autowired
    private TaskService taskService;

    @GetMapping
    public ResponseEntity saveTasks() {
        return new ResponseEntity("Data saved to .xlsx file successfully! "
                + reportService.save(taskService.getTasks()), HttpStatus.OK);
    }
}
