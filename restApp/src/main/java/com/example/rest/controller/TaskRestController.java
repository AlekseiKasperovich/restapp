package com.example.rest.controller;

import com.example.rest.model.Task;
import com.example.rest.service.TaskService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/tasks")
@RestController
public class TaskRestController {

    @Autowired
    private TaskService service;

    @PostMapping
    public ResponseEntity<Task> addTask(@RequestBody Task task) {
        return new ResponseEntity(service.saveTask(task), HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<List<Task>> findAllTasks() {
        return new ResponseEntity(service.getTasks(), HttpStatus.OK);
    }

    @GetMapping("name/{name}")
    public ResponseEntity<List<Task>> findAllTasksByNameContaining(@PathVariable String name) {
        return new ResponseEntity(service.getTasksByNameContaining(name),
                HttpStatus.OK);
    }

    @GetMapping("status/{status}")
    public ResponseEntity<List<Task>> findAllTasksByStatus(@PathVariable Integer status) {
        return new ResponseEntity(service.getTasksByStatus(status), HttpStatus.OK);
    }

    @GetMapping("date/{date1}/{date2}")
    public ResponseEntity<List<Task>> findAllTasksByDate(
            @PathVariable("date1") String date1, @PathVariable("date2") String date2) {
        return new ResponseEntity(service.getTasksByDate(date1, date2), HttpStatus.OK);
    }

    @GetMapping("{id}")
    public ResponseEntity<Task> findTaskById(@PathVariable Integer id) {
        Task task = service.getTaskById(id);
        if (task == null) {
            return new ResponseEntity("Task by ID " + id + " not found!",
                    HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(task, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<Task> updateTask(@RequestBody Task task) {
        return new ResponseEntity(service.updateTask(task), HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public ResponseEntity deleteTask(@PathVariable Integer id) {
        Task task = service.getTaskById(id);
        if (task == null) {
            return new ResponseEntity("Task by ID " + id + " not found!",
                    HttpStatus.NOT_FOUND);
        }
        service.deleteTask(id);
        return new ResponseEntity("Task " + id + " removed!", HttpStatus.OK);
    }

    @PostMapping(value = "{taskId}/items/{itemId}")
    public ResponseEntity<Task> addItemToTask(@PathVariable Integer taskId,
            @PathVariable Integer itemId) {
        Task task = service.getTaskById(taskId);
        if (task == null) {
            return new ResponseEntity("Task by ID " + taskId + " not found!",
                    HttpStatus.NOT_FOUND);
        }
        Task newTask = service.addItemToTask(task, itemId);
        if (newTask == null) {
            return new ResponseEntity("Task Item by ID " + itemId + " not found!",
                    HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(newTask, HttpStatus.OK);
    }

    @DeleteMapping(value = "{taskId}/items/{itemId}")
    public ResponseEntity<Task> removeItemFromTask(@PathVariable Integer taskId,
            @PathVariable Integer itemId) {
        Task task = service.getTaskById(taskId);
        if (task == null) {
            return new ResponseEntity("Task by ID " + taskId + " not found!",
                    HttpStatus.NOT_FOUND);
        }
        Task newTask = service.removeItemFromTask(task, itemId);
        if (newTask == null) {
            return new ResponseEntity("Task Item by ID " + itemId + " not found!",
                    HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(newTask, HttpStatus.OK);
    }
}
