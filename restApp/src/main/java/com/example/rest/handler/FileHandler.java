package com.example.rest.handler;

import com.example.rest.model.Report;
import com.example.rest.model.Task;
import com.example.rest.model.TaskItem;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;

@Component
public class FileHandler {

    private static final Logger LOGGER = LogManager.getLogger(FileHandler.class);

    private static XSSFCellStyle createStyleForTitle(XSSFWorkbook workbook) {
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        XSSFCellStyle style = workbook.createCellStyle();
        style.setFont(font);
        return style;
    }

    private static String createFile(List<Task> tasks) {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Tasks and items");

        int rowNum = 0;
        Cell cell;
        Row row;

        XSSFCellStyle style = createStyleForTitle(workbook);
        row = sheet.createRow(rowNum);

        cell = row.createCell(0, CellType.STRING);
        cell.setCellValue("task_id");
        cell.setCellStyle(style);
        cell = row.createCell(1, CellType.STRING);
        cell.setCellValue("name");
        cell.setCellStyle(style);
        cell = row.createCell(2, CellType.STRING);
        cell.setCellValue("description");
        cell.setCellStyle(style);
        cell = row.createCell(3, CellType.STRING);
        cell.setCellValue("start_date");
        cell.setCellStyle(style);
        cell = row.createCell(4, CellType.STRING);
        cell.setCellValue("end_date");
        cell.setCellStyle(style);
        cell = row.createCell(5, CellType.STRING);
        cell.setCellValue("status");
        cell.setCellStyle(style);
        cell = row.createCell(6, CellType.STRING);
        cell.setCellValue("item_id");
        cell.setCellStyle(style);
        cell = row.createCell(7, CellType.STRING);
        cell.setCellValue("name");
        cell.setCellStyle(style);
        cell = row.createCell(8, CellType.STRING);
        cell.setCellValue("description");
        cell.setCellStyle(style);
        cell = row.createCell(9, CellType.STRING);
        cell.setCellValue("value");
        cell.setCellStyle(style);
        cell = row.createCell(10, CellType.STRING);
        cell.setCellValue("task_id");
        cell.setCellStyle(style);

        for (Task task : tasks) {
            rowNum++;
            row = sheet.createRow(rowNum);
            cell = row.createCell(0, CellType.NUMERIC);
            cell.setCellValue(task.getTaskId());
            cell = row.createCell(1, CellType.STRING);
            cell.setCellValue(task.getName());
            cell = row.createCell(2, CellType.STRING);
            cell.setCellValue(task.getDescription());
            cell = row.createCell(3, CellType.STRING);
            cell.setCellValue(task.getStartDate().toString());
            cell = row.createCell(4, CellType.STRING);
            cell.setCellValue(task.getEndDate().toString());
            cell = row.createCell(5, CellType.NUMERIC);
            cell.setCellValue(task.getStatus());

            List<TaskItem> items = task.getItems();
            for (int i = 0; i < items.size(); i++) {
                TaskItem item = items.get(i);
                cell = row.createCell(6, CellType.NUMERIC);
                cell.setCellValue(item.getItemId());
                cell = row.createCell(7, CellType.STRING);
                cell.setCellValue(item.getName());
                cell = row.createCell(8, CellType.STRING);
                cell.setCellValue(item.getDescription());
                cell = row.createCell(9, CellType.STRING);
                cell.setCellValue(item.getValue());
                cell = row.createCell(10, CellType.NUMERIC);
                cell.setCellValue(item.getTask().getTaskId());
                if (items.size() > 1 && i != items.size() - 1) {
                    rowNum++;
                    row = sheet.createRow(rowNum);
                }
            }
        }
        File file = new File("D:/restApp/report.xslx");
        file.getParentFile().mkdirs();
        FileOutputStream outFile = null;
        try {
            outFile = new FileOutputStream(file);
        } catch (FileNotFoundException ex) {
            LOGGER.error(ex);
        }
        try {
            workbook.write(outFile);
        } catch (IOException ex) {
            LOGGER.error(ex);
        } finally {
            try {
                if (outFile != null) {
                    outFile.close();
                }
            } catch (IOException ex) {
                LOGGER.error(ex);
            }
        }
        return "Created file: " + file.getAbsolutePath();
    }

    public String writeToXLSX(Report report) {
        return createFile(report.getTasks());
    }
}
